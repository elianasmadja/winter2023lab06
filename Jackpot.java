import java.util.Scanner;
public class Jackpot{
    public static void main(String[]args){
        System.out.println("Welcome user!");
        System.out.println("Today you will be playing Jackpot!");
        System.out.println("Let's start!");
        
        Board game = new Board();
        boolean gameOver = false; 
        int numOfTilesClosed = 0;
        
        while(!gameOver){
            System.out.println(game);
            boolean newRoll = game.playATurn();
            if(newRoll == true){
                gameOver = true;
            }
            else{
                numOfTilesClosed++;
            }
        }
        
        if(numOfTilesClosed >= 7){
            System.out.println("You have reached the jackpot and won!!");
        }
        else{
            System.out.println("Sadly you have lost. Too bad");
        }
        
        
    }
    
    
    
}